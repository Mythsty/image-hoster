from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import ImageSerializer
from .models import Image
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
class ImageUploadView(APIView):
    seriailizer_class = ImageSerializer
    parser_classes = (MultiPartParser,)
    permission_classes = [IsAuthenticatedOrReadOnly]
    def put(self,request,*args, **kwargs):
        
        # if User.objects.get(username=)
        if request.user.is_authenticated:
            serializer = ImageSerializer(data=request.data)     
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data,status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'error' : 'Unathorized'},status=status.HTTP_401_UNAUTHORIZED)
    def get(self,request,*args, **kwargs):
        images = Image.objects.all()
        serializer = ImageSerializer(images, many=True)
        return Response(serializer.data)
    
