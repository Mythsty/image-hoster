from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.conf import settings
import os
# Create your models here.
def val_img_ext(filename: str):
    ext = str(filename).split('.')[-1]
    valid_exts = ['jpg','jpeg','png']
    if ext.lower() not in valid_exts:
        raise ValidationError("Image must be an png or jpg.")

def get_upload_dir(instance,image_path):
    image_name = str(image_path).split('/')[-1]
    relative_path = 'images/user_{0}/{1}/'.format(instance.user.id, image_name.split('.')[0])
    root_path = os.path.join(settings.MEDIA_ROOT, relative_path)
    print(root_path)
    if not os.path.exists(root_path):
        os.makedirs(root_path)
    return root_path + image_name


class Image(models.Model):
    
    class Meta:
        ordering = ['added']
    
    file = models.ImageField(upload_to=get_upload_dir,validators=[val_img_ext,])
    user = models.ForeignKey(User,on_delete=models.CASCADE,null=True,related_name='images')
    added = models.DateTimeField(auto_now_add=True)
    is_thumbnail = models.BooleanField(default=False)
    original = models.ForeignKey("self", related_name='thumbnails',on_delete=models.CASCADE,null=True)
    
    @classmethod
    def create_thumbnail(cls):
        instructions = cls.user.proxy_user.get().tier.img_instruct 
        print(instructions)
        print(type(instructions))
        
    
class Tier(models.Model): 
    
    tier_name = models.CharField(max_length=10)
    img_instruct = models.JSONField(null=False,default={})
    
    def set_instructions(self, **kwargs):
        pass    

    def save(self, *args, **kwargs):
        self.tier_name = type(self).__name__
        self.set_instructions()
        super().save(*args, **kwargs)
    def __str__(self):
        return self.tier_name + " Tier"
    

class TierManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(tier_name=self.model.__name__)
    
class CustomTierManager(models.Manager): 
    builtin_tiers = ['Basic','Premium','Enterprise']
    def get_queryset(self):
        return super().get_queryset().all().exclude(tier_name__in=self.builtin_tiers)

        
class Basic(Tier):
    
    class Meta: 
        proxy = True
    
    def set_instructions(self,**kwargs):
        self.image_processing_instruct = {
            'thumbnails' : [
                200,
            ],
        }

    
    objects = TierManager()
    
class Premium(Tier):
    
    class Meta: 
        proxy = True

    def set_instructions(self,**kwargs):
        self.image_processing_instruct = {
            'thumbnails' : [
                200,
                400,
            ],
            'original' : True,
        }
        
    objects = TierManager()
    
class Enterprise(Tier):
    
    class Meta: 
        proxy = True       

    def set_instructions(self,**kwargs):
        self.image_processing_instruct = {
            'thumbnails' : [
                200,
                400,
            ],
            'original' : True,
            'expiring_link' : True,
        }
        
    
class Custom(Tier):
    
    class Meta:
        proxy = True
    
    def set_instructions(self, **kwargs):
        
        self.image_processing_instruct = {
            'thumbnails' : 'arbitrary',
            'original' : True,
            'expiring_link' : True,
        }
        
    def save(self, *args, **kwargs):
        if 'tier_name' not in kwargs:
            super().save(*args, **kwargs)
        else:
            super(Tier,self).save(*args, **kwargs)
    
    objects = CustomTierManager()

class User_with_Tier(User):
    user = models.OneToOneField(User,null=False,on_delete=models.CASCADE, related_name='proxy_user')
    tier = models.ForeignKey(Tier,null=False,on_delete=models.PROTECT, related_name='users')
    
    