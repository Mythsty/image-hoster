A simple image hoster written in django.


The default database is implemented in postgresql with details in the settings.py.

1. Install required libraries:
`pip install requirements.txt`

2. If you want to use postgres create the database:
`CREATE DATABASE image_hosting;`

3. Create a superuser account:
`python manage.py createsuperuser`

4. Migrate the models:
`python manage.py makemigrations` 
 and 
`python manage.py migrate`

5. Run the web-app:
`python manage.py runserver`

