from django.shortcuts import render,redirect
from django.urls import reverse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login,logout

# Create your views here.
def loginView(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request,user)
            return redirect('images:upload')
        
    else:
        form = AuthenticationForm()
    
    return render(request,'accounts/login.html', {'form':form})

def logoutView(request):
    user = request.user
    if user.is_authenticated:
        logout(request)
    return redirect(reverse('accounts:login'))